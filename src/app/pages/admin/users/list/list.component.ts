import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { UsersService } from '../../../../services/admin/users.service';
import { User } from "../../../../interfaces/user";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";
import {response} from "express";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  users: User[]=[];
  public page: number | undefined;
  userAuthenticated:User=<User>{};

  constructor(private titleService: Title,
              private toastService: ToastService,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private storage: StorageService) {
    this.userAuthenticated = this.storage.getUser();
    this.titleService.setTitle("Lista de Usuarios");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getUsers().subscribe(response => {
      this.users = response.users;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site')
      console.log(error);
      this.loading.hide();
    });

  }

  active(user: string, value: boolean){
    this.loading.show();
    if (value){
      this.service.desActiveUser(user).subscribe(response => {
        this.showSuccess(`El usuario ${response.user.name} se desactivo con exíto`);
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      })
    } else {
      this.service.activeUser(user).subscribe(response => {
        this.showSuccess(`El usuario ${response.user.name} se activo con exíto`);
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    }
  }

  delete(user: string){
    this.loading.show();
    this.service.deleteUser(user).subscribe(response => {
      this.showSuccess(`El usuario ${response.user.name} se elimino con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
