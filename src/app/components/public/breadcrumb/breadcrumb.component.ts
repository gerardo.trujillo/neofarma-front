import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-breadcrumb-public',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbPublicComponent implements OnInit {

  @Input() titlePage: string = '';
  @Input() backPage: string = '';
  @Input() url: string = '';
  @Input() single: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
