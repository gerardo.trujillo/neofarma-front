import { Component, OnInit } from '@angular/core';
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { NewsService } from "../../../services/public/news.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService } from "../../../services/components/toast.service";
import { News } from "../../../interfaces/news";

@Component({
  selector: 'app-news-all',
  templateUrl: './news-all.component.html',
  styleUrls: ['./news-all.component.sass']
})
export class NewsAllComponent implements OnInit {

  news:News[]=[];
  faArrowRight = faArrowRight;

  constructor(private newsService: NewsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(){
    this.loading.show();
    this.newsService.getNews().subscribe(response => {
      this.news = response.news;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
