import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from "../../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.backUrl+ 'auth';
  public isLoggedIn  = false;

  constructor(private http: HttpClient) {
  }

  login(params: any){
    return this.http.post<any>(this.baseUrl + '/login', params);
  }

  signIn() {
    this.isLoggedIn = true;
  }

  getIsLoggedIn(): boolean{
    return this.isLoggedIn;
  }

  signOut() {
    localStorage.clear();
    this.isLoggedIn = false;
  }

  getBoolean(option: string): boolean{
    if (option == 'true'){
      return true;
    } else {
      return false;
    }
  }

}
