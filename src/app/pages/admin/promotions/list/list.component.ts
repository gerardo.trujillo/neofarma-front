import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { Title } from "@angular/platform-browser";
import { ToastService } from "../../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from "../../../../services/storage/storage.service";
import { PromotionsService } from "../../../../services/admin/promotions.service";
import { Promotion } from "../../../../interfaces/promotion";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  promotions:Promotion[]=[];
  public page: number | undefined;

  constructor(private titleService: Title,
              private toastService: ToastService,
              private service: PromotionsService,
              private loading: NgxSpinnerService,
              public storage: StorageService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getPromotions().subscribe(response => {
      this.promotions = response.promotions;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }
  active(promotion: string){
    this.loading.show();
    this.service.activePromotion(promotion).subscribe(response => {
      this.showSuccess(`La promoción ${response.promotion.name} se activo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        if (error.error.errors.length > 0){
          for (let i=0; error.error.errors.length>i;i++){
            this.showDanger(error.error.errors[0].msg);
          }
        } else {
          this.showDanger(error.error.msg);
        }
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  desActive(promotion: string){
    this.loading.show();
    this.service.desActivePromotion(promotion).subscribe(response => {
      this.showSuccess(`La promoción ${response.promotion.name} se desactivo con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        if (error.error.errors.length > 0){
          for (let i=0; error.error.errors.length>i;i++){
            this.showDanger(error.error.errors[0].msg);
          }
        } else {
          this.showDanger(error.error.msg);
        }
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  delete(promotion: string){
    this.loading.show();
    this.service.deletePromotion(promotion).subscribe(response => {
      this.showSuccess(`La promoción ${response.promotion.name} se elimino con exíto`);
      this.getData();
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
