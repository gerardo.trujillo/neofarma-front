import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.sass']
})
export class AboutUsComponent implements OnInit {

  constructor(private title: Title) {
    this.title.setTitle('¿Quienes Somos?')
  }

  ngOnInit(): void {
  }

}
