import { Component, OnInit } from '@angular/core';
import {Product} from "../../../interfaces/product";
import {Category} from "../../../interfaces/category";
import {NgxSpinnerService} from "ngx-spinner";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductsService} from "../../../services/public/products.service";
import {ToastService} from "../../../services/components/toast.service";
import {CategoriesService} from "../../../services/public/categories.service";

@Component({
  selector: 'app-products-all',
  templateUrl: './products-all.component.html',
  styleUrls: ['./products-all.component.sass']
})
export class ProductsAllComponent implements OnInit {

  products:Product[]=[];
  category:Category=<Category>{
    _id: '',
    parent_id: '',
    name: {
      es: '',
      en: '',
    },
    slug: '',
    content: {
      es: '',
      en: '',
    },
    type: '',
    image: '',
    active: false,
    status: false,
  };
  public page: number | undefined;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private router: Router,
              private serviceProducts: ProductsService,
              private toastService: ToastService,
              private service: CategoriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.getData();
    });
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.serviceProducts.getDataProductsAll().subscribe(response => {
      // console.log(response);
      this.products = response.products;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
