import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-privacy-notice',
  templateUrl: './privacy-notice.component.html',
  styleUrls: ['./privacy-notice.component.sass']
})
export class PrivacyNoticeComponent implements OnInit {

  constructor(private title: Title) {
    this.title.setTitle('Aviso de Privacidad');
  }

  ngOnInit(): void {
  }

}
