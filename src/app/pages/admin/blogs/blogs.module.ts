import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogsRoutingModule } from './blogs-routing.module';
import { BlogsComponent } from './blogs.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { NgxDropzoneModule } from "ngx-dropzone";
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";


@NgModule({
  declarations: [
    BlogsComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
    imports: [
      CommonModule,
      BlogsRoutingModule,
      NgbModule,
      FontAwesomeModule,
      NgxPaginationModule,
      FormsModule,
      CKEditorModule,
      NgxDropzoneModule,
      AdminComponentsModule
    ]
})
export class BlogsModule { }
