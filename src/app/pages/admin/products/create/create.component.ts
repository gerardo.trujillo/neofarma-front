import { Component, OnInit } from '@angular/core';
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ToastService} from "../../../../services/components/toast.service";
import {CategoriesService} from "../../../../services/admin/categories.service";
import {StorageService} from "../../../../services/storage/storage.service";



@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  alert:Alert = <Alert>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  categories: Category[]= [];
  kinds: any[]= [];
  routes: any[]= [];
  categoriesSelect: any[]=[];
  kindsSelect: any[]=[];
  routesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private categoriesService: CategoriesService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("Crear Producto");
  }

  ngOnInit(): void {
    this.getData();
    this.getDataKinds();
    this.getDataRoutes();
  }

  getData(){
    this.loading.show();
    this.categoriesService.getCategories().subscribe(response => {
      this.categories = response.categories.filter((item: {parent_id:string}) => item.parent_id == null);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getDataKinds(){
    this.loading.show();
    this.service.getKinds().subscribe(response => {
      this.kinds = response.kinds.filter((item: {parent_id:string}) => item.parent_id == null);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getDataRoutes(){
    this.loading.show();
    this.service.getRoutes().subscribe(response => {
      this.routes = response.routes;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('formula', form.value.formula);
    params.append('record', form.value.record);
    params.append('dose', form.value.dose);
    // params.append('use', form.value.use);
    params.append('indications', form.value.indications);
    params.append('routes', JSON.stringify(this.routesSelect));
    params.append('warnings', form.value.warnings);
    params.append('presentations', form.value.presentations);
    // params.append('characteristics', form.value.characteristics);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    params.append('kinds', JSON.stringify(this.kindsSelect));
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.postProduct(params).subscribe(response => {
      console.log(response);
      if (language == 'es'){
        this.showSuccess(`El producto ${response.product.name.es} se creo con exito`);
      } else if(language == 'en'){
        this.showSuccess(`El producto ${response.product.name.en} se creo con exito`);
      }
      this.router.navigateByUrl('/admin/products');
      this.loading.hide();
    }, error => {
      if (error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  push(id: string){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
    console.log(this.categoriesSelect);
  }

  pushKind(id: string){
    let index = this.kindsSelect.findIndex(item => item == id);
    if (index < 0){
      this.kindsSelect.push(id);
    } else {
      this.kindsSelect.splice(index, 1);
    }
    console.log(this.kindsSelect);
  }

  pushRoute(id: string){
    let index = this.routesSelect.findIndex(item => item == id);
    if (index < 0){
      this.routesSelect.push(id);
    } else {
      this.routesSelect.splice(index, 1);
    }
    console.log(this.routesSelect);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
