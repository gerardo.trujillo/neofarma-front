import { Component, OnInit } from '@angular/core';
import { faTable, faPlus } from '@fortawesome/free-solid-svg-icons';
import { NgxSpinnerService } from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import { environment } from "../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { TranslationService } from "../../../../services/admin/translation.service";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.backUrl;
  categories:any[]=[];
  public page: number | undefined;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: CategoriesService,
              public serviceTranslation: TranslationService,
              private toastService: ToastService,
              public storage: StorageService) {
    this.titleService.setTitle("Lista de Categorias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getCategories().subscribe(response => {
      this.categories = response.categories.filter((item: {parent_id:string}) => item.parent_id == null);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(category: string, value: boolean){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    if (value){
      this.service.desActiveCategory(category).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`La categoria ${response.category.name.es} se desactivo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`La categoria ${response.category.name.en} se desactivo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    } else {
      this.service.activeCategory(category).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`La categoria ${response.category.name.es} se activo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`La categoria ${response.category.name.en} se activo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    }
  }

  delete(category: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.deleteCategory(category).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`La categoria ${response.category.name.es} se elimino con exito`);
      } else if (language == 'en'){
        this.showSuccess(`La categoria ${response.category.name.en} se elimino con exito`);
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }


  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.categories = this.categories.filter(item => regex.test(item.name.es));
    } else {
      this.categories = this.categories.filter(item => regex.test(item.name.en));
    }
  }


  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}
