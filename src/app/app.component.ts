import {Component, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnDestroy{
  title = 'NeoFarma';

  ngOnDestroy() {
    localStorage.clear();
  }
}
