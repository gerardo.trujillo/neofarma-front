import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer, faPowerOff, faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Alert } from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { NgForm } from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { Category } from "../../../../interfaces/category";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { Image } from "../../../../interfaces/image";
import { environment } from "../../../../../environments/environment";
import { Gallery } from "../../../../interfaces/gallery";
import { StorageService } from "../../../../services/storage/storage.service";
import { ToastService } from "../../../../services/components/toast.service";
import { UploadsService } from "../../../../services/admin/uploads.service";
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;
  language = localStorage.getItem('language');
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  category: Category = <Category>{
    name:{
      es: 'nombre',
      en: 'name'
    },
    content:{
      es: 'descripción',
      en: 'description'
    }
  };
  gallery: Gallery = <Gallery>{};
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  images: any[] = [];
  files: File[] = [];

  constructor(private activatedRoute: ActivatedRoute,
              public serviceTranslation: TranslationService,
              private titleService: Title,
              public uploads: UploadsService,
              private toastService: ToastService,
              private serviceImage: GalleriesService,
              private service: CategoriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private storage: StorageService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  getData(id: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.showCategory(id).subscribe(response => {
      this.category = response.category;
      this.imageInit = response.category.image;
      this.images = response.category.images;
      if (language == 'es') {
        this.titleService.setTitle('Editar categoria ' + this.category.name.es);
      } else if(language == 'en'){
        this.titleService.setTitle('Editar categoria ' + this.category.name.en);
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.category._id, 'categories', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('content', form.value.description);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('language', language);
    this.service.putCategory(this.category._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`La categoria ${response.category.name.es} se actualizo con exito`);
      } else {
        this.showSuccess(`La categoria ${response.category.name.en} se actualizo con exito`);
      }
      this.router.navigateByUrl('/admin/categories');
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('categories', this.category._id, img).subscribe(response => {
      console.log(response);
      this.getData(this.category._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  activeImage(gallery: number){
    this.loading.show();
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}
