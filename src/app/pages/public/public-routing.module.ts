import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./main/main.module').then(m => m.MainModule)
      },
      {
        path: 'aviso-privacidad',
        loadChildren: () => import('./privacy-notice/privacy-notice.module').then(m => m.PrivacyNoticeModule)
      },
      {
        path: 'quienes-somos',
        loadChildren: () => import('./about-us/about-us.module').then(m => m.AboutUsModule)
      },
      {
        path: 'categorias/:category',
        loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesModule)
      },
      {
        path: 'categorias/productos/:category/:product',
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'noticias/:slug',
        loadChildren: () => import('./news/news.module').then(m => m.NewsModule)
      },
      {
        path: 'noticias',
        loadChildren: () => import('./news-all/news-all.module').then(m => m.NewsAllModule)
      },
      {
        path: 'todos-productos',
        loadChildren: () => import('./products-all/products-all.module').then(m => m.ProductsAllModule)
      },
      {
        path: 'todos-productos/:product',
        loadChildren: () => import('./product-all/product-all.module').then(m => m.ProductAllModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
      },
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
