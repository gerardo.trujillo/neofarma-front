import { Component, OnInit } from '@angular/core';
import { environment } from "../../../../environments/environment";
import { NgxSpinnerService } from "ngx-spinner";
import { DomSanitizer, Meta, Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastService } from "../../../services/components/toast.service";
import { NewsService } from "../../../services/public/news.service";
import { Gallery } from "angular-gallery";
import { News } from "../../../interfaces/news";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass']
})
export class NewsComponent implements OnInit {

  news:News=<News>{
    _id: '',
    title: {
      es: '',
      en: '',
    },
    slug: '',
    url: '',
    intro: {
      es: '',
      en: '',
    },
    content: {
      es: '',
      en: '',
    },
    date: '',
    datePicker: {
      year: 0,
      month: 0,
      day: 0
    },
    image: '',
    active: false,
    type: '',
    images: [],
  };
  images:any;
  url_images = environment.backUrl + 'uploads/news/'
  url = environment.frontUrl;
  linkface:any;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private router: Router,
              private meta: Meta,
              private sanitizer: DomSanitizer,
              private toastService: ToastService,
              private service: NewsService,
              private gallery: Gallery,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['slug']);
    });
  }

  ngOnInit(): void {
  }

  getData(slug: string){
    this.loading.show();
    this.service.showNews(slug).subscribe(response => {
      this.news = response.news;
      this.images = response.news.images;
      this.title.setTitle(this.news.title.es);
      let description = this.news.content.es.replace(/<[^>]*>?/g, '');
      this.meta.addTags([
        { property: 'og:url', content: this.url + '/noticias/' + slug },
        { property: 'og:type', content: 'website' },
        { property: 'og:description', content: description },
        { property: 'og:title', content: this.news.title.es },
        { property: 'og:image', content: this.url_images + this.news._id }
      ]);
      let link = 'https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fbeta.labneofarma.com%2Fnoticias%2F'
        + slug + '&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=307675644319584';
      this.linkface = this.sanitizer.bypassSecurityTrustResourceUrl(link);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showGallery(index: number) {
    let images:any[]=[];
    images.push({path: this.url_images + this.news._id})
    for(let i=0; i<this.images.length; i++){
      images.push({path: this.url_images + this.news._id + '/' + this.images[i]})
    }
    let prop = {
      images: images,
      index
    };
    this.gallery.load(prop);
  }

}
