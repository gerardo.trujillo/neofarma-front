import { Component, OnInit } from '@angular/core';
import { faList, faPowerOff, faTrashAlt, faSave, faEye, faEyeSlash, faImages, faEdit } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { environment } from "../../../../../environments/environment";
import { Title} from "@angular/platform-browser";
import { ProductsService } from "../../../../services/admin/products.service";
import { Category } from "../../../../interfaces/category";
import { Product } from "../../../../interfaces/product";
import { Gallery } from "../../../../interfaces/gallery";
import { Image } from "../../../../interfaces/image";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { CategoriesService } from "../../../../services/admin/categories.service";
import { ToastService } from "../../../../services/components/toast.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {UploadsService} from "../../../../services/admin/uploads.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faEdit = faEdit;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  gallery: Gallery = <Gallery>{};
  images: any[] = [];
  image:any;
  imageInit:any;
  thumbnail:any;
  product:Product = <Product>{
    _id: '',
    name: {
      es: '',
      en: '',
    },
    slug: '',
    formula: {
      es: '',
      en: '',
    },
    use: {
      es: '',
      en: '',
    },
    dose: {
      es: '',
      en: '',
    },
    presentations: {
      es: '',
      en: '',
    },
    indications: {
      es: '',
      en: '',
    },
    warnings: {
      es: '',
      en: '',
    },
    characteristics: {
      es: '',
      en: '',
    },
    image: '',
    active: false,
    categories: [],
    kinds: [],
    routes: [],
    images: [],
    record: ''
  };
  categories: Category[]= [];
  kinds: any[]= [];
  routes: any[]= [];
  categoriesSelect: any[]=[];
  kindsSelect: any[]=[];
  routesSelect: any[]=[];
  editImage = true;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              public uploads: UploadsService,
              private categoriesService: CategoriesService,
              private router: Router,
              private titleService: Title,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getCategories(){
    this.loading.show();
    this.categoriesService.getCategories().subscribe(response => {
      this.categories = response.categories.filter((item: {parent_id:string}) => item.parent_id == null);
      for (let i=0;this.categories.length>i;i++){
        for (let p=0;this.product.categories.length>p;p++){
          if (this.product.categories[p]._id == this.categories[i]._id){
            this.categories[i].status = true;
          }
        }
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }
  getRoutes(){
    this.loading.show();
    this.service.getRoutes().subscribe(response => {
      this.routes = response.routes;
      for (let i=0;this.routes.length>i;i++){
        for (let p=0;this.product.routes.length>p;p++){
          if (this.product.routes[p]._id == this.routes[i]._id){
            this.routes[i].status = true;
          }
        }
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getKinds(){
    this.loading.show();
    this.service.getKinds().subscribe(response => {
      this.kinds = response.kinds.filter((item: {parent_id:string}) => item.parent_id == null);
      for (let i=0;this.kinds.length>i;i++){
        for (let p=0;this.product.kinds.length>p;p++){
          if (this.product.kinds[p]._id == this.kinds[i]._id){
            this.kinds[i].status = true;
          }
        }
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getData(id: string){
    this.loading.show();
    this.service.getProduct(id).subscribe(response => {
      this.product = response.product;
      this.imageInit = response.product.image;
      this.images = response.product.images;
      for (let c=0;this.product.categories.length>c;c++){
        this.categoriesSelect.push(this.product.categories[c]._id);
      }
      for (let k=0;this.product.kinds.length>k;k++){
        this.kindsSelect.push(this.product.kinds[k]._id);
      }
      for (let r=0;this.product.routes.length>r;r++){
        this.routesSelect.push(this.product.routes[r]._id);
      }
      this.getCategories();
      this.getKinds();
      this.getRoutes();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.product._id, 'products', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('name', form.value.name);
    params.append('formula', form.value.formula);
    params.append('record', form.value.record);
    params.append('dose', form.value.dose);
    // params.append('use', form.value.use);
    params.append('indications', form.value.indications);
    params.append('routes', JSON.stringify(this.routesSelect));
    params.append('warnings', form.value.warnings);
    params.append('presentations', form.value.presentations);
    // params.append('characteristics', form.value.characteristics);
    params.append('categories', JSON.stringify(this.categoriesSelect));
    params.append('kinds', JSON.stringify(this.kindsSelect));
    params.append('language', language);
    if (this.image){
      params.append('file', this.image);
    }
    this.service.putProduct(this.product._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`El producto ${response.product.name.es} se actualizo con exito`);
      } else if(language == 'en'){
        this.showSuccess(`El producto ${response.product.name.en} se actualizo con exito`);
      }
      this.router.navigateByUrl('/admin/products');
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }


  push(id: string){
    let index = this.categoriesSelect.findIndex(item => item == id);
    if (index < 0){
      this.categoriesSelect.push(id);
    } else {
      this.categoriesSelect.splice(index, 1);
    }
  }

  pushKind(id: string){
    let index = this.kindsSelect.findIndex(item => item == id);
    if (index < 0){
      this.kindsSelect.push(id);
    } else {
      this.kindsSelect.splice(index, 1);
    }
  }

  pushRoute(id: string){
    let index = this.routesSelect.findIndex(item => item == id);
    if (index < 0){
      this.routesSelect.push(id);
    } else {
      this.routesSelect.splice(index, 1);
    }
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  deleteImageGallery(img: string){
    this.loading.show();
    this.uploads.deleteUploadGallery('products', this.product._id, img).subscribe(response => {
      this.getData(this.product._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('products', this.product._id, img).subscribe(response => {
      this.getData(this.product._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  deleteImageThumbnail(){
    this.thumbnail = undefined;
    this.image = undefined;
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
