import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyNoticeRoutingModule } from './privacy-notice-routing.module';
import { PrivacyNoticeComponent } from './privacy-notice.component';
import {PublicComponentsModule} from "../../../components/public/public.module";


@NgModule({
  declarations: [
    PrivacyNoticeComponent
  ],
    imports: [
        CommonModule,
        PrivacyNoticeRoutingModule,
        PublicComponentsModule
    ]
})
export class PrivacyNoticeModule { }
