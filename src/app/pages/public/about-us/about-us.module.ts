import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
import {PublicComponentsModule} from "../../../components/public/public.module";


@NgModule({
  declarations: [
    AboutUsComponent
  ],
    imports: [
        CommonModule,
        AboutUsRoutingModule,
        PublicComponentsModule
    ]
})
export class AboutUsModule { }
