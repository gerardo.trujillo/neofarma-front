import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsAllRoutingModule } from './news-all-routing.module';
import { NewsAllComponent } from './news-all.component';
import {PipesModule} from "../../../pipes/pipes.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {PublicComponentsModule} from "../../../components/public/public.module";


@NgModule({
  declarations: [
    NewsAllComponent
  ],
    imports: [
        CommonModule,
        NewsAllRoutingModule,
        PipesModule,
        FontAwesomeModule,
        PublicComponentsModule
    ]
})
export class NewsAllModule { }
