import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from "../../components/shared/shared.module";
import { NgxSpinnerModule } from "ngx-spinner";
import { AdminComponentsModule } from "../../components/admin/admin-components.module";



@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    NgxSpinnerModule,
    AdminComponentsModule
  ]
})
export class AdminModule { }
