import { Component, OnInit } from '@angular/core';
import { TranslateService} from '@ngx-translate/core';
import { faLanguage } from '@fortawesome/free-solid-svg-icons';
import { TranslationService } from "../../../services/admin/translation.service";
import {ComponentsService} from "../../../services/admin/components.service";

@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.sass']
})
export class TranslationComponent implements OnInit {

  faLanguage = faLanguage;
  activeLang = 'es';


  constructor(private translate: TranslateService,
              public serviceMenu: ComponentsService,
              public service: TranslationService) {
    if (typeof this.activeLang === 'string') {
      this.translate.setDefaultLang(this.activeLang);
    }
  }

  ngOnInit(): void {
  }

  public changeLenguage(lang: string){
    this.activeLang = lang;
    this.translate.use(lang);
    localStorage.setItem('language', lang);
    this.service.setLang(lang);
  }

}
