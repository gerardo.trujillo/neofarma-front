import { Component, OnInit } from '@angular/core';
import { faEnvelope, faPhone, faMapMarkedAlt, faAddressCard, faShareAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  faEnvelope = faEnvelope;
  faAddressCard = faAddressCard;
  faMapMarkedAlt = faMapMarkedAlt;
  faShareAlt = faShareAlt;
  faPhone = faPhone;

  constructor() { }

  ngOnInit(): void {
  }

}
