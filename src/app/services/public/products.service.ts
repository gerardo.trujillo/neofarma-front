import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  baseUrl = environment.backUrl + 'products/public';

  constructor(private http: HttpClient) {

  }

  getDataProducts(slug: string){
    return this.http.get<any>(`${this.baseUrl}/${slug}`);
  }
  getDataProductsAll(){
    return this.http.get<any>(`${this.baseUrl}`);
  }

  getProduct(slug: string){
    return this.http.get<any>(`${this.baseUrl}/show/${slug}`);
  }

}
