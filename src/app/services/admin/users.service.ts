import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url = environment.backUrl + 'users';
  token:string='';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }

  getUsers(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  showUser(id: string){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  activeUser(id: string){
    return this.http.get<any>(this.url + '/active/' + id, {headers: this.headers});
  }

  desActiveUser(id: string){
    return this.http.get<any>(this.url + '/des-active/' + id, {headers: this.headers});
  }

  postUser(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putUser(id: string, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteUser(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
