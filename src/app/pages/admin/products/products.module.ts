import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxPaginationModule } from "ngx-pagination";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxDropzoneModule } from "ngx-dropzone";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { CategoriesComponent } from './categories/categories.component';
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";
import {PipesModule} from "../../../pipes/pipes.module";
import { KindsComponent } from './kinds/kinds.component';


@NgModule({
  declarations: [
    ProductsComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    CategoriesComponent,
    KindsComponent
  ],
    imports: [
        CommonModule,
        ProductsRoutingModule,
        FontAwesomeModule,
        NgxPaginationModule,
        NgbModule,
        AdminComponentsModule,
        NgxDropzoneModule,
        FormsModule,
        CKEditorModule,
        PipesModule
    ]
})
export class ProductsModule { }
