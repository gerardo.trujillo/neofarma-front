import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { NgxPaginationModule } from "ngx-pagination";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { SubcateriesCategoriesComponent } from "./subcateries-categories/subcateries-categories.component";
import { FormsModule } from "@angular/forms";
import { NgxDropzoneModule } from "ngx-dropzone";
import { CreateSubcategoriesComponent } from "./create-subcategories/create-subcategories.component";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { AdminComponentsModule } from "../../../components/admin/admin-components.module";
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    CategoriesComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    SubcateriesCategoriesComponent,
    CreateSubcategoriesComponent
  ],
    imports: [
        CommonModule,
        CategoriesRoutingModule,
        NgxPaginationModule,
        FontAwesomeModule,
        NgbModule,
        FormsModule,
        NgxDropzoneModule,
        AdminComponentsModule,
        CKEditorModule,
        PipesModule
    ]
})
export class CategoriesModule { }
