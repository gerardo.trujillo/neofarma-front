import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { User } from "../../../../interfaces/user";
import { Alert } from "../../../../interfaces/alert";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { UsersService } from "../../../../services/admin/users.service";
import { NgForm } from "@angular/forms";
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { StorageService } from "../../../../services/storage/storage.service";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  // @ts-ignore
  @ViewChild('checkbox') checkbox: ElementRef;
  user: User = <User>{};
  faEdit = faEdit;
  type = 'password';
  faPassword = faEye
  passwordV= false;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private toastService: ToastService,
              private storage: StorageService,
              private router: Router) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.service.showUser(id).subscribe(response => {
      this.user = response.user;
      this.titleService.setTitle("Editar a " + this.user.name);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  chagePassword(){
    if (this.checkbox.nativeElement.checked){
      this.passwordV= true;
    } else {
      this.passwordV= false;
    }
  }

  submit(form: NgForm){
    let checkPassword = true;
    if (this.passwordV){
      if (form.value.password != form.value.passwordRepeat){
        this.showDanger('Las contraseñas deben coincidir');
        checkPassword = false;
      }
    }
    if (checkPassword){
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', form.value.name);
      params.append('email', form.value.email);
      params.append('password', form.value.password);
      this.service.putUser(this.user.uid, params).subscribe( response => {
        this.showSuccess(`El usuario ${response.user.name} se actualizo con exíto`);
        this.router.navigateByUrl('/admin/users');
        this.loading.hide();
      }, error => {
        console.log(error);
        if (error.status = 400){
          console.log(error.error);
          this.showDanger(error.error.msg);
        } else {
          this.showDanger('Se encontro un error al ingresar al usuario, comunicate con el administrador del web-site');
        }
        this.loading.hide();
      });
    }

  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
