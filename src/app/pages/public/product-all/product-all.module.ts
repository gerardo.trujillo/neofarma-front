import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductAllRoutingModule } from './product-all-routing.module';
import { ProductAllComponent } from './product-all.component';
import {PublicComponentsModule} from "../../../components/public/public.module";
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    ProductAllComponent
  ],
  imports: [
    CommonModule,
    ProductAllRoutingModule,
    PublicComponentsModule,
    PipesModule
  ]
})
export class ProductAllModule { }
