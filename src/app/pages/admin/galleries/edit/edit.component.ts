import { Component, OnInit } from '@angular/core';
import { faList, faPowerOff, faTrashAlt, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { environment} from "../../../../../environments/environment";
import { Alert} from "../../../../interfaces/alert";
import { TranslationService } from "../../../../services/admin/translation.service";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import {Gallery} from "../../../../interfaces/gallery";
import {Image} from "../../../../interfaces/image";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faTrashAlt = faTrashAlt;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = localStorage.getItem('language');
  url_images = environment.backUrl;
  alert:Alert = <Alert>{};
  gallery:Gallery=<Gallery>{};
  images:Image[]=[];
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: GalleriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
    this.getData(params['id']);
  });
  }

  ngOnInit(): void {
  }

  getData(id:number){
    let galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
    let images = JSON.parse(<string>localStorage.getItem('images_admin'));
    this.gallery = galleries.find((item: {id:number}) => item.id == id);
    this.images = images.filter((item: {gallery_id:number}) => item.gallery_id == this.gallery.id);
   if(this.serviceTranslation.getLang() == 'es'){
     this.titleService.setTitle('Editar Galleria '+ this.gallery.name.es);
   } else if (this.serviceTranslation.getLang() == 'en'){
     this.titleService.setTitle('Editar Galleria '+ this.gallery.name.en);
   }
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = <string>this.serviceTranslation.getLang();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('language', language);
    params.append('type', this.gallery.type);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putGallery(this.gallery.id, params).subscribe( response => {
      let images = response.images;
      let gallery = response.gallery;
      const galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
      const index = galleries.findIndex((item: { id: number; }) => item.id == gallery.id);
      galleries[index] = gallery;
      galleries[index].active = true;
      localStorage.setItem('galleries_admin', JSON.stringify(galleries));
      let imagesAll = JSON.parse(<string>localStorage.getItem('images_admin'));
      for (let i=0; images.length>i; i++){
        images[i].active = true;
        imagesAll.push(images[i]);
        console.log(imagesAll);
      }
      localStorage.setItem('images_admin', JSON.stringify(imagesAll));
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/galleries');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
      console.log(err);
    });
  }

  closed() {
    this.alert.active = false;
  }

  setTime(){
    setTimeout(() => {
      this.closed()
    },10000);
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.service.deleteImage(gallery).subscribe( response => {
      let imagesAll = JSON.parse(<string>localStorage.getItem('images_admin'));
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      // @ts-ignore
      const indexAll = imagesAll.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      imagesAll.splice(indexAll, 1);
      localStorage.setItem('images_admin', JSON.stringify(imagesAll));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.service.activeImage(gallery).subscribe( response => {
      let imagesAll = JSON.parse(<string>localStorage.getItem('images_admin'));
      const image = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == image.id);
      // @ts-ignore
      const indexAll = imagesAll.findIndex(item => item.id == response.id);
      this.images[index].active = response.active;
      imagesAll[indexAll].active = response.active;
      localStorage.setItem('images_admin', JSON.stringify(imagesAll));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador',
          active: true
        };
      } else {
        this.loading.hide();
        this.alert = {
          type: 'danger',
          message: 'Se detecto un error comunicate con el administrador',
          active: true
        };
      }
    });
  }

}
