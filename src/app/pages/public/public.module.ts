import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import {PublicComponentsModule} from "../../components/public/public.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgxSpinnerModule} from "ngx-spinner";
import {SharedModule} from "../../components/shared/shared.module";


@NgModule({
  declarations: [
    PublicComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    PublicComponentsModule,
    FontAwesomeModule,
    NgxSpinnerModule,
    SharedModule
  ]
})
export class PublicModule { }
