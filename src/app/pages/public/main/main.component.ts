import { Component, OnInit } from '@angular/core';
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { NewsService } from "../../../services/public/news.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService } from "../../../services/components/toast.service";
import { News } from "../../../interfaces/news";
import { MainService } from "../../../services/public/main.service";
import { Promotion } from "../../../interfaces/promotion";
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  promotions:Promotion[]=[];
  news:News[]=[];
  faArrowRight = faArrowRight;

  constructor(private newsService: NewsService,
              private meta: Meta,
              private mainService: MainService,
              private loading: NgxSpinnerService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.meta.addTags([
      { property: 'og:url', content: "https://labneofarma.com"},
      { property: 'og:type', content: 'website' },
      { property: 'og:description', content: "Somos un laboratorio veterinario con mas de 40 años dedicados a desarrollar soluciones integrales para mejorar la salud animal. " },
      { property: 'og:title', content: "Lab Neo Farma" },
    ]);
    this.getPromotions();
    this.getNews();
  }

  getPromotions(){
    this.loading.show();
    this.mainService.getPromotions().subscribe(response => {
      this.promotions = response.promotions;
      this.loading.hide();
      console.log(this.news);
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getNews(){
    this.loading.show();
    this.newsService.getNews().subscribe(response => {
      this.news = response.news;
      this.loading.hide();
      console.log(this.news);
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
