import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class KindsService {

  url = environment.backUrl + 'kinds';
  token = '';
  headers:any;

  constructor(private http: HttpClient,
              private storage: StorageService) {
    this.token = this.storage.getToken();
    this.headers = new HttpHeaders({
      'x-token': this.token,
    });
  }


  getKinds(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  showCategory(id: string){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  getKindChildren(id: string){
    return this.http.get<any>(this.url + '/children/' + id, {headers: this.headers});
  }

  parentKind(id: string){
    return this.http.get<any>(this.url + '/parent/' + id, {headers: this.headers});
  }

  activeCategory(id: string){
    return this.http.get<any>(this.url + '/active/' + id, {headers: this.headers});
  }

  desActiveCategory(id: string){
    return this.http.get<any>(this.url + '/des-active/' + id , {headers: this.headers});
  }

  postCategory(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putCategory(id: string, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteCategory(id: string){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
