import { Component, OnInit } from '@angular/core';
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.sass']
})
export class PublicComponent implements OnInit {

  faWhatsapp = faWhatsapp;

  constructor() { }

  ngOnInit(): void {
  }

}
