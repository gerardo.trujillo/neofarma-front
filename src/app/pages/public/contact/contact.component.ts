import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {NgxSpinnerService} from "ngx-spinner";
import {ToastService} from "../../../services/components/toast.service";
import {EmailsService} from "../../../services/public/emails.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  recaptcha = false;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: EmailsService,
              private toastService: ToastService) {
    this.titleService.setTitle('Contacto');
  }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    if(!this.recaptcha){
      this.showDanger('Debe comprobar que no es robot');
    } else {
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', form.value.name);
      params.append('email', form.value.email);
      params.append('telephone', form.value.telephone);
      params.append('message', form.value.message);
      this.service.sendContact(params).subscribe(response => {
        console.log(response);
        this.showSuccess('El Mensaje se envio con exito');
        this.loading.hide();
      }, error => {
        console.log(error)
        this.showDanger('Se encontro un error al enviar el mensaje');
        this.loading.hide();
      });
    }
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    if(captchaResponse != null){
      this.recaptcha = true;
    } else {
      this.recaptcha = false;
    }
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
