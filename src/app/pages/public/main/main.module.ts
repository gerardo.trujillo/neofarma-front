import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {PipesModule} from "../../../pipes/pipes.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [
    MainComponent
  ],
    imports: [
        CommonModule,
        MainRoutingModule,
        NgbModule,
        PipesModule,
        FontAwesomeModule
    ]
})
export class MainModule { }
