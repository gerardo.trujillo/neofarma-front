import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public language = 'es';

  constructor() { }

  setLang(lang: string){
    this.language = lang;
    localStorage.setItem('language', lang);
  }

  getLang(): string{
    if (!localStorage.getItem('language')) {
      return this.language;
    } else {
      return <string>localStorage.getItem('language')
    }
  }
}
