import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { News } from "../../../../interfaces/news";
import { Alert } from "../../../../interfaces/alert";
import { environment } from "../../../../../environments/environment";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { TranslationService } from "../../../../services/admin/translation.service";
import { BlogsService } from "../../../../services/admin/blogs.service";
import { ToastService } from "../../../../services/components/toast.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  blogs: News[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};
  url_images = environment.backUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private service: BlogsService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Noticias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getBlogs().subscribe(response => {
      if(response.ok){
        this.blogs = response.blogs;
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(id: string){
    this.loading.show();
    this.service.activeBlog(id).subscribe(response => {
      if(response.ok){
        if (response.blog.active) {
          this.showSuccess('El blog ' + response.blog.title.es + ' se activo con exito');
        } else {
          this.showSuccess('El blog ' + response.blog.title.es + ' se desactivo con exito');
        }
        this.getData();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  delete(id: string){
    this.loading.show();
    this.service.deleteBlog(id).subscribe(response => {
      if(response.ok){
        this.showSuccess('El blog ' + response.blog.title.es + ' se elimino con exito');
        this.getData();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }


  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
