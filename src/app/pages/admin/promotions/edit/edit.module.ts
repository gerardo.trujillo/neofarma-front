import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {FormsModule} from "@angular/forms";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {PipesModule} from "../../../../pipes/pipes.module";
import {AdminComponentsModule} from "../../../../components/admin/admin-components.module";


@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    CommonModule,
    EditRoutingModule,
    FontAwesomeModule,
    FormsModule,
    CKEditorModule,
    PipesModule,
    AdminComponentsModule
  ]
})
export class EditModule { }
