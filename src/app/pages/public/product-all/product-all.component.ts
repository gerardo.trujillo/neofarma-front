import { Component, OnInit } from '@angular/core';
import {Product} from "../../../interfaces/product";
import {environment} from "../../../../environments/environment";
import {Category} from "../../../interfaces/category";
import {NgxSpinnerService} from "ngx-spinner";
import {DomSanitizer, Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastService} from "../../../services/components/toast.service";
import {ProductsService} from "../../../services/public/products.service";
import {CategoriesService} from "../../../services/public/categories.service";
import {Gallery} from "angular-gallery";

@Component({
  selector: 'app-product-all',
  templateUrl: './product-all.component.html',
  styleUrls: ['./product-all.component.sass']
})
export class ProductAllComponent implements OnInit {

  product:Product = <Product>{
    _id: '',
    name: {
      es: '',
      en: '',
    },
    slug: '',
    formula: {
      es: '',
      en: '',
    },
    use: {
      es: '',
      en: '',
    },
    dose: {
      es: '',
      en: '',
    },
    presentations: {
      es: '',
      en: '',
    },
    indications: {
      es: '',
      en: '',
    },
    warnings: {
      es: '',
      en: '',
    },
    characteristics: {
      es: '',
      en: '',
    },
    image: '',
    active: false,
    categories: [],
    kinds: [],
    routes: [],
    images: [],
    record: ''
  };
  images:any;
  url_images = environment.backUrl + 'uploads/products/'
  url = environment.frontUrl;
  linkface:any;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private router: Router,
              private meta: Meta,
              private sanitizer: DomSanitizer,
              private toastService: ToastService,
              private service: ProductsService,
              private serviceCategory: CategoriesService,
              private gallery: Gallery,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['product']);
    });
  }

  ngOnInit(): void {
  }

  getData(slug: string){
    this.loading.show();
    this.service.getProduct(slug).subscribe(response => {
      this.product = response.product;
      this.images = response.product.images;
      this.title.setTitle(this.product.name.es);
      let description = this.product.dose.es.replace(/<[^>]*>?/g, '');
      this.meta.addTags([
        { property: 'og:url', content: this.url + '/todos-productos/' + slug },
        { property: 'og:type', content: 'website' },
        { property: 'og:description', content: description },
        { property: 'og:title', content: this.product.name.es },
        { property: 'og:image', content: this.url_images + this.product._id }
      ]);
      let link = 'https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Flabneofarma.com%2Ftodos-productos%2F'
        + slug + '&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=307675644319584';
      this.linkface = this.sanitizer.bypassSecurityTrustResourceUrl(link);
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showGallery(index: number) {
    let images:any[]=[];
    images.push({path: this.url_images + this.product._id})
    for(let i=0; i<this.images.length; i++){
      images.push({path: this.url_images + this.product._id + '/' +  this.images[i]})
    }
    let prop = {
      images: images,
      index,
      objectFit: 'contain',
      counter: true,
    };
    this.gallery.load(prop);
  }

}
