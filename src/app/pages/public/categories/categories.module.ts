import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import {PipesModule} from "../../../pipes/pipes.module";
import {PublicComponentsModule} from "../../../components/public/public.module";
import {NgxPaginationModule} from "ngx-pagination";


@NgModule({
  declarations: [
    CategoriesComponent
  ],
    imports: [
        CommonModule,
        CategoriesRoutingModule,
        PipesModule,
        PublicComponentsModule,
        NgxPaginationModule
    ]
})
export class CategoriesModule { }
