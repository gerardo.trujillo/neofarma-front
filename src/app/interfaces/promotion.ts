export interface Promotion {
  _id: string;
  title: string;
  image: string;
  name: string;
  url: string;
  description: string;
  active: boolean;
  delete: boolean;
}
