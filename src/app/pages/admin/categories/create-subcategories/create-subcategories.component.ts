import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgForm } from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { TranslationService } from '../../../../services/admin/translation.service';
import { CategoriesService } from '../../../../services/admin/categories.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { ParentCategory } from "../../../../interfaces/parent-category";
import { StorageService } from "../../../../services/storage/storage.service";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-create-subcategories',
  templateUrl: './create-subcategories.component.html',
  styleUrls: ['./create-subcategories.component.sass']
})
export class CreateSubcategoriesComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  parent: ParentCategory = <ParentCategory>{
    name: {
      es: 'name',
      en: 'name'
    }
  };
  language = localStorage.getItem('language');
  url_images = localStorage.getItem('url_images');
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:string = '';
  name:string = '';
  description:string = '';
  editImage = true;


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: CategoriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private toastService: ToastService,
              private activatedRoute: ActivatedRoute,
              private storage: StorageService) {
        this.activatedRoute.params.subscribe( params => {
          this.id = params['parent'];
          this.parentShow(params['parent']);
      });
  }

  ngOnInit(): void {
  }

  parentShow(parent: string){
    this.loading.show();
    this.service.showCategory(parent).subscribe(response => {
      this.parent = response.category;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('content', form.value.description);
    params.append('language', language);
    if (this.image){
      params.append('file', this.image);
    }
    params.append('parent_id', this.id);
    this.service.postCategory(params).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`La categoria ${response.category.name.es} se creo con exito`);
        } else {
          this.showSuccess(`La categoria ${response.category.name.en} se creo con exito`);
        }
        this.router.navigateByUrl('/admin/categories');
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }


  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light' });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light' });
  }

}
