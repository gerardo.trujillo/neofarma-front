export const environment = {
  production: true,
  backUrl: 'https://backend.labneofarma.com/api/',
  frontUrl: 'https://labneofarma.com/',
};
