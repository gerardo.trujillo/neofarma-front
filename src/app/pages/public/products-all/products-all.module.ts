import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsAllRoutingModule } from './products-all-routing.module';
import { ProductsAllComponent } from './products-all.component';
import {NgxPaginationModule} from "ngx-pagination";
import {PublicComponentsModule} from "../../../components/public/public.module";
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    ProductsAllComponent
  ],
  imports: [
    CommonModule,
    ProductsAllRoutingModule,
    NgxPaginationModule,
    PublicComponentsModule,
    PipesModule
  ]
})
export class ProductsAllModule { }
