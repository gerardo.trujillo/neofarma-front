import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import {NgxPaginationModule} from "ngx-pagination";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {PipesModule} from "../../../../pipes/pipes.module";
import {AdminComponentsModule} from "../../../../components/admin/admin-components.module";


@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    ListRoutingModule,
    NgxPaginationModule,
    FontAwesomeModule,
    PipesModule,
    AdminComponentsModule,
  ]
})
export class ListModule { }
