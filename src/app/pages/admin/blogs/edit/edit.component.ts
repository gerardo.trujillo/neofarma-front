import { Component, OnInit } from '@angular/core';
import { environment} from "../../../../../environments/environment";
import { faList, faSave, faEye, faPowerOff, faTrashAlt, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgbCalendar, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NgxSpinnerService } from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import { News } from "../../../../interfaces/news";
import { Gallery } from "../../../../interfaces/gallery";
import { Image } from "../../../../interfaces/image";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { BlogsService } from "../../../../services/admin/blogs.service";
import { ToastService } from "../../../../services/components/toast.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faTrashAlt = faTrashAlt;
  faPowerOff = faPowerOff;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  blog:News=<News>{
    title: {
      es: 'title',
      en: 'title'
    },
    intro: {
      es: 'intro',
      en: 'intro'
    },
    content: {
      es: 'content',
      en: 'content'
    }
  };
  image:any;
  gallery:Gallery=<Gallery>{};
  images:Image[]=[];
  imageInit:any;
  thumbnail:any;
  editImage = true;
  model: NgbDateStruct = <NgbDateStruct>{};
  date: { year: number; month: number; } | undefined;
  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: BlogsService,
              private toastService: ToastService,
              private serviceImage: GalleriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private titleService: Title,
              private calendar: NgbCalendar,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {

  }

  getData(id: string){
    this.loading.show();
    this.service.showBlog(id).subscribe(response => {
      if (response.ok){
        this.blog = response.blog;
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let month = '';
    let day = '';
    let date = '';
    let language = this.serviceTranslation.getLang();
    if (this.isString(form.value.date)){
      date = form.value.date;
    } else {
      if (form.value.date.month < 10 ){
        month = '0'+form.value.date.month;
      } else {
        month = form.value.date.month;
      }
      if (form.value.date.day < 10){
        day = '0' + form.value.date.day;
      } else {
        day = form.value.date.day;
      }
      date = form.value.date.year + '-' + month + '-' + day;
    }
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('intro', form.value.intro);
    params.append('content', form.value.content);
    params.append('date', date);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    this.service.putBlog(this.blog._id, params).subscribe(response => {
      if (response.ok){
        this.showSuccess('El blog ' + response.blog.title.es + ' se actualizo con exito');
        this.router.navigateByUrl('/admin/blogs');
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  isString(text:any){
    if(typeof text === 'string' || text instanceof String){
      return true;
    }else{
      return false;
    }
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }
    reader.readAsDataURL(file);
  }


  deleteImage(gallery: number){
    this.loading.show();
  }

  activeImage(gallery: number){
    this.loading.show();
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
