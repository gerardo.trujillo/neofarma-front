import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarAdminComponent } from "./navbar/navbar.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { BreadcrumbAdminComponent } from "./breadcrumb/breadcrumb.component";
import { TranslationComponent } from "./translation/translation.component";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";



@NgModule({
  declarations: [
    NavbarAdminComponent,
    SidebarComponent,
    BreadcrumbAdminComponent,
    TranslationComponent
  ],
  exports: [
    NavbarAdminComponent,
    SidebarComponent,
    BreadcrumbAdminComponent,
    TranslationComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ]
})
export class AdminComponentsModule { }
