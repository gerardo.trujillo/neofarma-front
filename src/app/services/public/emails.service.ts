import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailsService {

  url = environment.backUrl + 'emails';
  token = '';
  headers:any;

  constructor(private http: HttpClient) {
  }

  sendContact(params: any){
    return this.http.post<any>(`${this.url}/send/contact`, params);
  }


}
