import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsAllComponent } from './news-all.component';

const routes: Routes = [{ path: '', component: NewsAllComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsAllRoutingModule { }
