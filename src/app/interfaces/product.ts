import {Category} from "./category";

export interface Product {
  _id: string;
  name: {
    es: string;
    en: string;
  };
  slug: string;
  formula: {
    es: string;
    en: string;
  };
  use: {
    es: string;
    en: string;
  };
  dose: {
    es: string;
    en: string;
  };
  presentations: {
    es: string;
    en: string;
  };
  indications: {
    es: string;
    en: string;
  };
  warnings: {
    es: string;
    en: string;
  };
  characteristics: {
    es: string;
    en: string;
  };
  image: string;
  record: string;
  active: boolean;
  categories: Category[];
  kinds: any[];
  routes: any[];
  images: any[];
}
