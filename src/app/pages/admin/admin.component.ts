import { Component, HostListener, OnInit} from '@angular/core';
import { Meta, Title} from "@angular/platform-browser";
import { ComponentsService } from "../../services/admin/components.service";
import { Alert } from "../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from "../../services/storage/storage.service";
import { Router } from "@angular/router";
import { AuthService } from "../../services/auth/auth.service";
import { UserIdleService } from 'angular-user-idle';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
      this.serviceMenu.setBars(true);
    } else if(window.innerWidth > 769) {
      this.serviceMenu.setClose(false);
      this.serviceMenu.setBars(false);
    }
  }
  alert:Alert=<Alert>{};
  idleState='';
  timedOut=false;

  constructor(private title: Title,
              private meta: Meta,
              private loading: NgxSpinnerService,
              public serviceMenu: ComponentsService,
              private serviceAuth: AuthService,
              private storage: StorageService,
              private router: Router,
              private userIdle: UserIdleService) {
    this.title.setTitle('Administrador');
  }

  ngOnInit(): void {
    //Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => console.log(count));

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      this.stop();
      this.stopWatching();
      this.logout();
    });
  }

  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }

  logout(){
    this.serviceAuth.signOut();
    this.router.navigate(['/sign/login']);
  }

}
