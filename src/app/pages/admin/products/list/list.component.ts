import { Component, OnInit } from '@angular/core';
import { Alert } from "../../../../interfaces/alert";
import { Title } from "@angular/platform-browser";
import { NgxSpinnerService } from "ngx-spinner";
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { ProductsService } from "../../../../services/admin/products.service";
import { Product } from "../../../../interfaces/product";
import { environment } from "../../../../../environments/environment";
import { TranslationService } from "../../../../services/admin/translation.service";
import {ToastService} from "../../../../services/components/toast.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  products: Product[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};
  url_images = environment.backUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private service: ProductsService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Productos");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
   this.loading.show();
    this.service.getProducts().subscribe(response => {
      this.products = response.products;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  active(product: string, value: boolean){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    if (value){
      this.service.desActiveProduct(product).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`El producto ${response.product.name.es} se desactivo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`El producto ${response.product.name.en} se desactivo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    } else {
      this.service.activeProduct(product).subscribe(response => {
        if (language == 'es'){
          this.showSuccess(`El producto ${response.product.name.es} se activo con exito`);
        } else if (language == 'en'){
          this.showSuccess(`El producto ${response.product.name.en} se activo con exito`);
        }
        this.getData();
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
        console.log(error);
        this.loading.hide();
      });
    }
  }

  delete(id: string){
    this.loading.show();
    let language = this.serviceTranslation.getLang();
    this.service.deleteProduct(id).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`El producto ${response.product.name.es} se elimino con exito`);
      } else if (language == 'en'){
        this.showSuccess(`El producto ${response.product.name.en} se elimino con exito`);
      }
      this.getData();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  new(id: string){
    this.loading.show();
    this.service.newProduct(id).subscribe(response => {
      if (response.ok){
        if (response.product.new){
          this.showSuccess('EL producto ' + response.product.name.es + ' se catalogo como nuevo con exito');
        } else {
          this.showSuccess('EL producto ' + response.product.name.es + ' se quito de nuevo con exito');
        }
        this.getData();
      }
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
