import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { environment} from "../../../../../environments/environment";
import { faList, faSave, faEye, faPowerOff, faTrashAlt, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { TranslationService } from "../../../../services/admin/translation.service";
import { NewsService } from "../../../../services/admin/news.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import { News } from "../../../../interfaces/news";
import { GalleriesService } from "../../../../services/admin/galleries.service";
import { ToastService } from "../../../../services/components/toast.service";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {UploadsService} from "../../../../services/admin/uploads.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  // @ts-ignore
  @ViewChild('dp') dp: ElementRef;
  faList = faList;
  faHandPointer = faHandPointer;
  faTrashAlt = faTrashAlt;
  faPowerOff = faPowerOff;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  news:News=<News>{
    title: {
      es: '',
      en: ''
    },
    intro: {
      es: '',
      en: ''
    },
    content: {
      es: '',
      en: ''
    }
  };
  image:any;
  images:any[]=[];
  imageInit:any;
  thumbnail:any;
  editImage = true;
  date: { year: number; month: number; } | undefined;
  datePicker: any;
  files: File[] = [];
  latest_date:any;

  constructor(public serviceTranslation: TranslationService,
              private service: NewsService,
              private serviceImage: GalleriesService,
              private loading: NgxSpinnerService,
              private toastService: ToastService,
              private router: Router,
              public uploads: UploadsService,
              private titleService: Title,
              private ngbCalendar: NgbCalendar,
              private dateAdapter: NgbDateAdapter<string>,
              private calendar: NgbCalendar,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['id']);
      console.log(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.service.showNews(id).subscribe(response => {
      this.news = response.news;
      this.imageInit = response.news.image;
      this.images = response.news.images;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  today() {
    return this.dateAdapter.toModel(this.ngbCalendar.getToday())!;
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    // @ts-ignore
    params.append('file', ...event.addedFiles);
    this.uploads.postUploadGallery(this.news._id, 'news', params).subscribe(response => {
      this.showSuccess('La imagen se agrego con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    let month = '';
    let day = '';
    let date = '';
    let language = this.serviceTranslation.getLang();
    if (this.isString(form.value.date)){
      date = form.value.date;
    } else {
      if (form.value.date.month < 10 ){
        month = '0'+form.value.date.month;
      } else {
        month = form.value.date.month;
      }
      if (form.value.date.day < 10){
        day = '0' + form.value.date.day;
      } else {
        day = form.value.date.day;
      }
      date = form.value.date.year + '-' + month + '-' + day;
    }
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('url', form.value.url);
    params.append('intro', form.value.intro);
    params.append('content', form.value.content);
    params.append('date', date);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    this.service.putNews(this.news._id, params).subscribe(response => {
      if (language == 'es'){
        this.showSuccess(`La noticia ${response.news.title.es} se actualizo con exito`);
      } else {
        this.showSuccess(`La noticia ${response.news.title.en} se actualizo con exito`);
      }
      this.router.navigateByUrl('/admin/news');
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  isString(text:any){
    if(typeof text === 'string' || text instanceof String){
      return true;
    }else{
      return false;
    }
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }


  deleteImage(img: string){
    this.loading.show();
    this.uploads.deleteUpload('news', this.news._id, img).subscribe(response => {
      console.log(response);
      this.getData(this.news._id);
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
