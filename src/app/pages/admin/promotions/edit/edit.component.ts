import { Component, OnInit } from '@angular/core';
import { environment } from "../../../../../environments/environment";
import { faList, faSave, faEye, faEyeSlash, faImages, faHandPointer } from '@fortawesome/free-solid-svg-icons';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { ToastService } from "../../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from "@angular/router";
import { PromotionsService } from "../../../../services/admin/promotions.service";
import { Title } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";
import { UploadsService } from "../../../../services/admin/uploads.service";
import { Promotion } from "../../../../interfaces/promotion";

// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  faList = faList;
  faHandPointer = faHandPointer;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  url_images = environment.backUrl;
  public Editor = ClassicEditor;
  image:any;
  promotion:Promotion=<Promotion>{};
  imageInit:any;
  thumbnail:any;
  editImage = true;
  model: NgbDateStruct = <NgbDateStruct>{};
  date: { year: number; month: number; } | undefined;

  constructor(private toastService: ToastService,
              private loading: NgxSpinnerService,
              private router: Router,
              private service: PromotionsService,
              private uploads: UploadsService,
              private activatedRouter: ActivatedRoute,
              private titleService: Title) {
    this.titleService.setTitle("Editar Promoción");
    this.activatedRouter.params.subscribe(params => {
      this.getData(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getData(id: string){
    this.loading.show();
    this.service.showPromotion(id).subscribe(response => {
      this.promotion = response.promotion;
      this.imageInit = this.promotion.image;
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  submit(form: NgForm) {
    this.loading.show();
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('name', form.value.name);
    params.append('description', form.value.description);
    params.append('url', form.value.url);
    params.append('file', this.image);
    this.service.putPromotion(this.promotion._id, params).subscribe(response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/promotions');
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    const params= new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('file', this.image);
    this.uploads.putUpload('news', this.promotion._id, params).subscribe(response => {
      this.showSuccess('La imagen se actualizó con exito');
      this.loading.hide();
    }, error => {
      if(error.status == 400){
        this.showDanger(error.error.msg);
      } else {
        this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      }
      console.log(error);
      this.loading.hide();
    });
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }
    reader.readAsDataURL(file);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
