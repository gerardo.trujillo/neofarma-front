import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Title } from "@angular/platform-browser";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastService } from "../../../services/components/toast.service";
import { CategoriesService } from "../../../services/public/categories.service";
import { Product } from "../../../interfaces/product";
import { Category } from "../../../interfaces/category";
import { ProductsService } from "../../../services/public/products.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass']
})
export class CategoriesComponent implements OnInit {

  products:Product[]=[];
  category:Category=<Category>{
    _id: '',
    parent_id: '',
    name: {
      es: '',
      en: '',
    },
    slug: '',
    content: {
      es: '',
      en: '',
    },
    type: '',
    image: '',
    active: false,
    status: false,
  };
  public page: number | undefined;

  constructor(private loading: NgxSpinnerService,
              private title: Title,
              private router: Router,
              private serviceProducts: ProductsService,
              private toastService: ToastService,
              private service: CategoriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['category']);
      this.getCategory(params['category']);
    });
  }

  ngOnInit(): void {
  }

  getCategory(slug: string){
    this.loading.show();
    this.service.getCategory(slug).subscribe(response => {
      // console.log(response);
      this.category = response.category;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  getData(slug: string){
    this.loading.show();
    this.serviceProducts.getDataProducts(slug).subscribe(response => {
      // console.log(response);
      this.products = response.products;
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
