import { Component, OnInit } from '@angular/core';
import { Gallery } from "../../../../interfaces/gallery";
import { Alert } from "../../../../interfaces/alert";
import { environment } from "../../../../../environments/environment";
import { faTable, faPlus, faPowerOff, faTrashAlt, faCertificate, faStar } from '@fortawesome/free-solid-svg-icons';
import { Title} from "@angular/platform-browser";
import { NgxSpinnerService} from "ngx-spinner";
import { ProductsService } from "../../../../services/admin/products.service";
import { TranslationService } from "../../../../services/admin/translation.service";
import {GalleriesService} from "../../../../services/admin/galleries.service";
import {Product} from "../../../../interfaces/product";
import {News} from "../../../../interfaces/news";
import {Category} from "../../../../interfaces/category";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faCertificate = faCertificate;
  faStar = faStar;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  galleries: Gallery[]=[];
  categories: Category[]=[];
  products: Product[]=[];
  news: News[]=[];
  public page: number | undefined;
  alert: Alert = <Alert>{};
  url_images = environment.backUrl;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: GalleriesService,
              public serviceTranslation: TranslationService) {
    this.titleService.setTitle("Lista de Galerias");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.galleries = JSON.parse(<string>localStorage.getItem('galleries_admin'));
    this.products = JSON.parse(<string>localStorage.getItem('products_admin'));
    this.categories = JSON.parse(<string>localStorage.getItem('categories_admin'));
    this.news = JSON.parse(<string>localStorage.getItem('news_admin'));
  }

  active(id: number){
    this.loading.show();
  }

  delete(id: number){
    this.loading.show();

  }

  getReference(id: string, type:string){
    let gallery:any;
    if (id != null){
      switch (type){
        case 'categories':
          gallery = this.categories.find(item => item._id == id);
          break
        case 'products':
          gallery = this.products.find(item => item._id == id);
          break
        case 'news':
          gallery = this.news.find(item => item._id == id);
          break
      }
    } else {
      return 'La galería no pertenece a ningún elemento';

    }
    if (type != 'news'){
      if (this.serviceTranslation.getLang() == 'es'){
        return gallery.name.es
      } else if(this.serviceTranslation.getLang() == 'en'){
        return gallery.name.en
      }
    } else {
      if(this.serviceTranslation.getLang() == 'es'){
        return gallery.title.es;
      } else if (this.serviceTranslation.getLang() == 'en'){
        return gallery.title.en;
      }
    }
  }


  replaceImage(image: any){
    image.onerror = '';
    image.src = 'assets/images/missing.png'
  }

}
