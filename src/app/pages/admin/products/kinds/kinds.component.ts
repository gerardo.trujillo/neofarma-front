import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Kind } from "../../../../interfaces/kind";
import { NgxSpinnerService } from "ngx-spinner";
import { KindsService } from "../../../../services/admin/kinds.service";
import { ToastService } from "../../../../services/components/toast.service";

@Component({
  selector: 'app-products-kinds',
  templateUrl: './kinds.component.html',
  styleUrls: ['./kinds.component.sass']
})
export class KindsComponent implements OnInit {

  @Input() parent!: string;
  @Input() kindsSelect:Kind[]=[];
  // @ts-ignore
  @ViewChild(KindsComponent) KindsComponent: KindsComponent;
  kinds: Kind[] = [];
  @Output() kindSelect: EventEmitter<string>;

  constructor(private loading: NgxSpinnerService,
              private serviceKind: KindsService,
              private toastService: ToastService, ) {
    this.kindSelect = new EventEmitter();
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.serviceKind.getKindChildren(this.parent).subscribe(response => {
      this.kinds = response.kinds;
      this.activeKinds();
      this.loading.hide();
    }, error => {
      this.showDanger('Se encontro un error, comunicate con el administrador del web-site');
      console.log(error);
      this.loading.hide();
    });
  }

  activeKinds(){
    for(let p=0; this.kinds.length>p; p++){
      for (let l=0; this.kindsSelect.length>l; l++){
        if(this.kinds[p]._id == this.kindsSelect[l]._id){
          this.kinds[p].status = true;
        }
      }
    }
  }

  push(id: string){
    this.kindSelect.emit(id);
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
